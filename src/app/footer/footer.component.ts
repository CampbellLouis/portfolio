import {Component, OnInit} from '@angular/core';
import {trigger, transition, useAnimation} from '@angular/animations';
import {bounce} from 'ng-animate';
import {FormGroup, FormBuilder} from '@angular/forms';
import {WindowService} from '../window.service';
import {MailService, MailServiceResponse} from '../mail.service';
import swal from 'sweetalert';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass'],
  animations: [
    trigger('bounce', [transition('* => *', useAnimation(bounce))])
  ],
})
export class FooterComponent implements OnInit {
  bounce: any;
  contactForm: FormGroup;
  windowRef: any;

  constructor(private fb: FormBuilder, private win: WindowService, private mail: MailService) {}

  ngOnInit() {
    this.windowRef = this.win.windowRef;

    this.contactForm = this.fb.group({
      name: '',
      email: '',
      message: ''
    });
  }

  public sendContact(): void {
    this.mail.sendContactRequest(
      this.contactForm.controls.email.value,
      this.contactForm.controls.message.value,
      this.contactForm.controls.name.value
    ).subscribe(async (response: MailServiceResponse) => {
      if (response.success) {
        await swal('Your contact request was send', 'You will receive a confirmation to your e-mail address soon', 'success');
      } else {
        await swal('There was an error with your request.', response.errorMsg, 'error');
      }
    });
  }
}


