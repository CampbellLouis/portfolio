import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MailService {

  constructor(private http: HttpClient) { }

  public sendContactRequest(mail: string, message: string, name: string) {
    return this.http.post('https://europe-west1-portfolio-5f29c.cloudfunctions.net/contactMail', {
      name,
      mail,
      message
    });
  }

}

export interface MailServiceResponse {
  success: boolean;
  errorMsg: string;
}
