import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.sass']
})
export class PortfolioComponent implements OnInit {
  portfolioEntries: PortfolioEntry[] = [
    {
      title : 'Toggl',
      // tslint:disable-next-line:max-line-length
      description : 'Game developed during 72 hours for the Geta GameJam 9',
      url : 'https://floating-int.itch.io/toggl',
      sourceUrl : '',
      pictureUrl : 'assets/images/Portfolio/toggl.webp',
      category : Filters.Game,
      technologies : [
        {name : 'Unity', iconClass : 'devicon-laravel-plain'},
        {name : 'C Sharp', iconClass : 'devicon-csharp-plain'},
      ]
    },
    {
      title : 'Association des chargés de enseignement national a.s.b.l',
      // tslint:disable-next-line:max-line-length
      description : 'Website built for ACEN using PHP & Laravel on the backend. It includes a backend for user management and a locked of member section for verified members.',
      url : 'https://acen.lu',
      sourceUrl : '',
      pictureUrl : 'assets/images/Portfolio/Acen-squashed.webp',
      category : Filters.Web,
      technologies : [
        {name : 'Laravel', iconClass : 'devicon-laravel-plain'},
        {name : 'Bootstrap', iconClass : 'devicon-bootstrap-plain'},
        {name : 'Jquery', iconClass : 'devicon-jquery-plain'},
        {name : 'Mysql', iconClass : 'devicon-mysql-plain'}
      ]
    },
    {
      title : 'Personal Portfolio',
      // tslint:disable-next-line:max-line-length
      description : 'This website was built to showcase projects i have completed. It is hosted on Firebase hosting and uses Firebase cloud functions to send the emails through the contact form.\n The frontend is built using Angular.',
      url : 'https://campbell.lu',
      sourceUrl : 'https://bitbucket.org/CampbellLouis/portfolio/src/master/',
      pictureUrl : 'assets/images/Portfolio/Portfolio-squashed.webp',
      category : Filters.Web,
      technologies : [
        {name : 'Angular', iconClass : 'devicon-angularjs-plain'},
        {name : 'Bootstrap', iconClass : 'devicon-bootstrap-plain'},
        {name : 'NodeJs', iconClass : 'devicon-nodejs-plain'},
        {name : 'TypeScript', iconClass : 'devicon-typescript-plain'}
      ]
    },
    {
      title : 'Electro-Center Shop',
      description : 'Shop for ElectroCenter built using JTL Shop and SQL Server.',
      url : 'https://shop.electro-center.lu',
      sourceUrl : '',
      pictureUrl : 'assets/images/Portfolio/ElectroCenterShop-squashed.webp',
      category : Filters.Web,
      technologies : [
        {name : 'JTL Shop', iconClass : ''},
        {name : 'SQL Server', iconClass : ''},
        {name : 'Jquery', iconClass : 'devicon-jquery-plain'},
      ]
    },
    {
      title : 'Electro-Center Planing',
      // tslint:disable-next-line:max-line-length
      description : 'Planing Application built for Electro Center. It allows to assign construction sites to workgroups and manage worker time.',
      url : '#',
      sourceUrl : 'https://bitbucket.org/CampbellLouis/planing/src/master/',
      pictureUrl : 'assets/images/Portfolio/ElectroCenterPlaning-squashed.webp',
      category : Filters.Web,
      technologies : [
        {name : 'Laravel', iconClass : 'devicon-laravel-plain'},
        {name : 'UI Kit', iconClass : ''},
        {name : 'VueJs', iconClass : 'devicon-vuejs-plain'},
        {name : 'Mysql', iconClass : 'devicon-mysql-plain'}
      ]
    },
    {
      title : 'Code Stop',
      // tslint:disable-next-line:max-line-length
      description : 'Coding game built during my first year of BTS. The goal is to move the bus through the level using JavaScript. The game is only meant as a prototype and is not a complete game.',
      url : 'https://students.btsi.lu/camlo',
      sourceUrl : '',
      pictureUrl : 'assets/images/Portfolio/CodeStop-squashed.webp',
      category : Filters.Web,
      technologies : [
        {name : 'Materialize CSS', iconClass : ''},
        {name : 'TypeScript', iconClass : 'devicon-typescript-plain'},
        {name : 'Mysql', iconClass : 'devicon-mysql-plain'}
      ]
    },
    {
      title : 'Gaston',
      // tslint:disable-next-line:max-line-length
      description : 'Mobile Game with the goal of learning french verbs. It was a week long group project during the first year of the BTS Game Programming curriculum',
      url : 'https://students.btsi.lu/camlo',
      sourceUrl : 'https://bitbucket.org/CampbellLouis/gaston-xamarin/src/master/',
      pictureUrl : 'assets/images/Portfolio/Gaston-squashed.webp',
      category : Filters.Application,
      technologies : [
        {name : 'Xamarin', iconClass : ''},
        {name : 'Skia', iconClass : ''},
        {name : 'C#', iconClass : 'devicon-csharp-plain'}
      ]
    },
    {
      title : 'MathSkia',
      // tslint:disable-next-line:max-line-length
      description : 'A math project built to calculate projectile physics and show their trajectories. It is built using Xamarin Mac and the Skia Graphics Library',
      url : 'https://students.btsi.lu/camlo',
      sourceUrl : 'https://bitbucket.org/CampbellLouis/mathskia/src/master/',
      pictureUrl : 'assets/images/Portfolio/MathSkia-squashed.webp',
      category : Filters.Application,
      technologies : [
        {name : 'Xamarin', iconClass : ''},
        {name : 'Skia', iconClass : ''},
        {name : 'DotNet', iconClass : 'devicon-dot-net-plain'},
        {name : 'C#', iconClass : 'devicon-csharp-plain'},
      ]
    },
    {
      title : 'Arcadena',
      // tslint:disable-next-line:max-line-length
      description : 'This was a semester long project that included building an arcade game cabinet and a corresponding game. The programming was done by Alex Nogueira and me. The game features a qr based mobile login that can be used to authenticate a player.',
      url : 'https://students.btsi.lu/camlo',
      sourceUrl : '',
      pictureUrl : 'assets/images/Portfolio/Arcadena-squashed.webp',
      category : Filters.Game,
      technologies : [
        {name : 'Unity Engine', iconClass : ''},
        {name : 'C#', iconClass : 'devicon-csharp-plain'},
        {name : 'nodejs', iconClass : 'devicon-nodejs-plain'},
      ]
    },
    {
      title : '1 Million Fishy Sticks',
      // tslint:disable-next-line:max-line-length
      description : 'This is a game built in 24 hours during the Game of Code 2019 for the Dockler Holding Challenge. The goal is to pick up sushi and drop it into your plate without dropping it on the floor.',
      url : 'https://students.btsi.lu/camlo',
      sourceUrl : '',
      pictureUrl : 'assets/images/Portfolio/FishySticks-squashed.webp',
      category : Filters.Game,
      technologies : [
        {name : 'Unity Engine', iconClass : ''},
        {name : 'C#', iconClass : 'devicon-csharp-plain'},
      ]
    },
    {
      title : 'BedTimeIsDeadTime',
      // tslint:disable-next-line:max-line-length
      description : 'Game built during the Geta GameJam 8 in 72 hours. The goal of the game is to keep the monsters away from the characters bed using the flashlight.',
      url : 'https://xanlosh.itch.io/bedtime-is-deadtime',
      sourceUrl : '',
      pictureUrl : 'assets/images/Portfolio/BedTimeIsDeadTime-squashed.webp',
      category : Filters.Game,
      technologies : [
        {name : 'Unity Engine', iconClass : ''},
        {name : 'C#', iconClass : 'devicon-csharp-plain'},
      ]
    },
    {
      title : 'Culth\'s Bane',
      // tslint:disable-next-line:max-line-length
      description : 'Another GameJam Game built for the GMTK 2019 GameJam. It is a card game with randomly generated rooms with the goal of reaching the end without dying. The twist is that every card can only be used once and after each turn the rest of the hand is discarded leaving the player with the choice which card is going to be most beneficial.',
      url : 'https://students.btsi.lu/camlo',
      sourceUrl : '',
      pictureUrl : 'assets/images/Portfolio/CulthsBane-squashed.webp',
      category : Filters.Game,
      technologies : [
        {name : 'Unity Engine', iconClass : ''},
        {name : 'C#', iconClass : 'devicon-csharp-plain'},
      ]
    },

  ];
  filter: Filters = Filters.All;
  constructor() { }
  public changeFilter(filterId: number) {
    this.filter = filterId;
  }
  public isActive(filterId: number): boolean {
    return this.filter === filterId;
  }
  public isHidden(category: Filters) {
    if (this.filter !== Filters.All) {
      return this.filter !== category;
    }
    return false;
  }
  ngOnInit() {
  }

}
interface PortfolioEntry {
  title: string;
  description: string;
  url: string;
  sourceUrl: string;
  pictureUrl: string;
  category: Filters;
  technologies: TechnologyEntry[];
}

interface TechnologyEntry {
  name: string;
  iconClass: string;
}

enum Filters {
  'All',
  'Web',
  'Game',
  'Application'
}
