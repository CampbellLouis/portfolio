import * as functions from 'firebase-functions';
import * as sgMail from '@sendgrid/mail';
sgMail.setApiKey('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');
// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//



export const contactMail = functions.region('europe-west1').https.onRequest(async (req, res) => {
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Headers', 'Content-Type');
    if (req.body.message !== undefined && req.body.message !== '' && req.body.mail !== '' && req.body.mail !== undefined) {
        const msg = {
            to: req.body.mail,
            from: 'louis@campbell.lu',
            bcc: 'louis@campbell.lu',
            templateId: 'd-f5baa7aa5b8f41df9ba8147c32c09775',
            dynamic_template_data: {
                message: req.body.message,
                date: Date.now().toString
            }
        };

        await sgMail.send(msg);
        res.send({ 'success': true });
        return;
    }
    res.send({ 'success': false ,'errorMsg' : 'Please verify your form input'});
    return;
});
